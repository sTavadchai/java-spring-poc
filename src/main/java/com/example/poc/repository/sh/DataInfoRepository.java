package com.example.poc.repository.sh;

import com.example.poc.entity.sh.DataInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DataInfoRepository extends JpaRepository<DataInfo, String> {

}
