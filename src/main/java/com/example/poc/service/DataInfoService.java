package com.example.poc.service;

import com.example.poc.entity.sh.DataInfo;
import com.example.poc.repository.sh.DataInfoRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DataInfoService {

    DataInfoRepository dataInfoRepository;

    public List<DataInfo> getDataInfoAll() {
        return dataInfoRepository.findAll();
    }

    @Transactional
    public void saveDataInfoAll() {
        List<DataInfo> data = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            DataInfo t = new DataInfo();
            t.setCallLogId(UUID.randomUUID().toString());
            t.setCachedName("Test:" + i);
            t.setCallDatetime(new Date());
            t.setCallDuration("");
            t.setCallType("Type:" + i);
            t.setLotData(UUID.randomUUID().toString());
            t.setPhoneNumber("0000000000");
            t.setRowId(UUID.randomUUID().toString());
            data.add(t);
        }
        dataInfoRepository.saveAll(data);
    }
}
