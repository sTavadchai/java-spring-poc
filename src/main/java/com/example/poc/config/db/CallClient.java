package com.example.poc.config.db;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "callEntityManagerFactory",
        transactionManagerRef = "callTransactionManager",
        basePackages = {"com.example.poc.repository.call"})
public class CallClient {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.jpa.properties")
    public Map<String, String> callAppJpaProperties() {
        Map<String, String> properties = new HashMap<>();
        properties.put("hibernate.implicit_naming_strategy",
                "org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy");
        properties.put("hibernate.physical_naming_strategy",
                "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");

        return properties;
    }

    @Bean(name = "callDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.call")
    public DataSource callDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "callEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean callEntityManagerFactory(
            EntityManagerFactoryBuilder builder, @Qualifier("callDataSource") DataSource dataSource) {
        return builder.dataSource(dataSource).packages("com.example.poc.entity.call")
                .properties(callAppJpaProperties()).persistenceUnit("call").build();
    }

    @Bean(name = "callTransactionManager")
    public PlatformTransactionManager callTransactionManager(
            @Qualifier("callEntityManagerFactory") EntityManagerFactory callEntityManagerFactory) {
        return new JpaTransactionManager(callEntityManagerFactory);
    }
}
