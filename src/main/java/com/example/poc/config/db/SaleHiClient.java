package com.example.poc.config.db;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "shEntityManagerFactory",
        transactionManagerRef = "shTransactionManager",
        basePackages = {"com.example.poc.repository.sh"})
public class SaleHiClient {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.jpa.properties")
    public Map<String, String> shAppJpaProperties() {
        Map<String, String> properties = new HashMap<>();
        properties.put("hibernate.implicit_naming_strategy",
                "org.springframework.boot.orm.jpa.hibernate.SpringImplicitNamingStrategy");
        properties.put("hibernate.physical_naming_strategy",
                "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");

        return properties;
    }

    @Primary
    @Bean(name = "shDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.sh")
    public DataSource shDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "shEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean shEntityManagerFactory(
            EntityManagerFactoryBuilder builder, @Qualifier("shDataSource") DataSource dataSource) {
        return builder.dataSource(dataSource).packages("com.example.poc.entity.sh")
                .properties(shAppJpaProperties()).persistenceUnit("sh").build();
    }

    @Primary
    @Bean(name = "shTransactionManager")
    public PlatformTransactionManager shTransactionManager(
            @Qualifier("shEntityManagerFactory") EntityManagerFactory shEntityManagerFactory) {
        return new JpaTransactionManager(shEntityManagerFactory);
    }

}
