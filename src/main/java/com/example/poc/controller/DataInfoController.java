package com.example.poc.controller;

import com.example.poc.entity.sh.DataInfo;
import com.example.poc.service.DataInfoService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/v1/api")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DataInfoController {

    DataInfoService dateService;

    @GetMapping("/dataInfo")
    public ResponseEntity<List<DataInfo>> getDataInfoAll() {
        return new ResponseEntity<>(dateService.getDataInfoAll(), HttpStatus.OK);
    }

    @PutMapping("/dataInfo")
    public ResponseEntity<Object> putDataInfo() {
        Date st = new Date();
        System.out.println("Start: " + st);

        dateService.saveDataInfoAll();

        Date en = new Date();
        System.out.println("End: " + en);
        System.out.println((en.getTime() - st.getTime()) / 1000);

        return new ResponseEntity(HttpStatus.CREATED);
    }

}
