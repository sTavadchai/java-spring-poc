package com.example.poc.entity.sh;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Persistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "Data_Info")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataInfo implements Persistable<String> {
    @Id
    @Column(name = "Call_Log_Id")
    private String callLogId;

    @Column(name = "Cached_Name")
    private String cachedName;
    @Column(name = "Call_Datetime")
    private Date callDatetime;
    @Column(name = "Call_Duration")
    private String callDuration;
    @Column(name = "Call_Type")
    private String callType;
    @Column(name = "Lot_Data")
    private String lotData;
    @Column(name = "Phone_Number")
    private String phoneNumber;
    @Column(name = "Row_Id")
    private String rowId;
    @Column(name = "Transfer_Status")
    private String transferStatus;

    @Override
    public String getId() {
        return callLogId;
    }

    @Override
    public boolean isNew() {
        return true;
    }

}
